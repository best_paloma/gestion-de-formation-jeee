package app;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.app.model.Person;
import com.app.service.PersonService;

public class PersonTest {
	private ClassPathXmlApplicationContext context;
	@Before
	public void Setup() throws Exception{
		
	}
	@Test
	public void test() {
		context = new ClassPathXmlApplicationContext(new String[]{ "applicationContext.xml" });
		PersonService ps = (PersonService) context.getBean("personService");
		List<Person> list1 = ps.getAll();
		Person r1 = new Person();
		r1.setNom("Hamza");
		ps.add(r1);
		Person r2 = new Person();
		r2.setNom("John");
		ps.add(r2);

		List<Person> list2 = ps.getAll();
		assertTrue((list1.size()+2) == list2.size());
		
	}

}
