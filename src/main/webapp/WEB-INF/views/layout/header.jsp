<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="<c:url value="/assets/images/favicon.png" />" type="image/png">

  <title>Go Formation</title>

  <link href="<c:url value="/assets/css/style.default.css" />" rel="stylesheet">
  <link href="<c:url value="/assets/css/jquery.datatables.css" />" rel="stylesheet">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->
  
<c:if test="${not empty paramValues.stylesheets}">
	<c:forEach var="stylesheet" items="${paramValues.stylesheets}">
		<link href="<c:url value="${stylesheet}" />" rel="stylesheet">
	</c:forEach>
</c:if>

</head>

<body>

<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>