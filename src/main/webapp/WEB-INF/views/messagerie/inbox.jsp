<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<%! String menuActuel = "menu_messagerie";  %>

<jsp:include page="../../views/layout/header.jsp" />
<jsp:include page="../../views/layout/leftpanel.jsp" />
<jsp:include page="../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-envelope"></i> Messagerie <span>Boite de réception</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="/messagerie" />">Messagerie</a></li>
          <li class="active">Boite de réception</li>
        </ol>
      </div>
    </div>
    
    
        <div class="contentpanel panel-email">

        <div class="row">
        	<div class="col-sm-3 col-lg-2">
                <a href="<c:url value="/messagerie/compose" />" class="btn btn-danger btn-block btn-compose-email">Nouveau Message</a>
                
               
                <ul class="nav nav-pills nav-stacked nav-email">
                    <li class="active">
                    <a href="<c:url value="/messagerie/" />">
                        <!-- <span class="badge pull-right">2</span> -->
                        <i class="glyphicon glyphicon-inbox"></i> Reçus
                    </a>
                    </li>
                    <li><a href="<c:url value="/messagerie/outbox" />"><i class="glyphicon glyphicon-send"></i> Envoyees</a></li>
                   
                </ul>
                
                <div class="mb30"></div>
        	</div><!-- col-sm-3 -->
            <div class="col-sm-9 col-lg-10">
                
                <div class="panel panel-default">
                    <div class="panel-body">

                        <h5 class="subtitle mb5">Boite de réception</h5>
                        
                        <div class="table-responsive">
                            <table class="table table-email">
                              <tbody>
                              <c:forEach var="m" items="${inbox_messages}">
                              
                                <tr <c:if test="${m.isNotRead(user_id) == false}"> class="unread" </c:if>>
                                  
                                  <td>
                                    <div class="media" data-id="${m.id }">
                                        <div class="media-body">
                                            <span class="media-meta pull-right"><joda:format pattern="MM/dd/yyyy H:m:s" value="${m.dateMessage}" /></span>
                                            <h4 class="text-primary">${m.from.username }</h4>
                                            <small class="text-muted"></small>
                                            <p class="email-summary">${m.subject } </p>
                                        </div>
                                    </div>
                                  </td>
                                </tr>
                               </c:forEach>
                                

                              </tbody>
                            </table>
                        </div><!-- table-responsive -->
                        
                    </div><!-- panel-body -->
                </div><!-- panel -->
                
            </div><!-- col-sm-9 -->
            
        </div><!-- row -->
    
    </div>
    
    
<jsp:include page="../../views/layout/rightpanel.jsp" />
<jsp:include page="../../views/layout/footer.jsp" />

<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>

<script>

  jQuery(document).ready(function() {
	    jQuery("a#<%= menuActuel %>").parent("li").addClass("active");

    jQuery('#table1').dataTable({
        "sPaginationType": "full_numbers"
      });
    
 // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
 
 
 	// Read mail
    jQuery('.table-email .media').click(function(){
    	var id = $(this).data("id");
        location.href="<c:url value="/messagerie/view?id=" />"+id;
    });
    

  });
</script>

</body>
</html>