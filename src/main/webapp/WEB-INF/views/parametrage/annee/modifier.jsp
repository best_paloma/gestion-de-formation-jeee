<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>


<%! String menuActuel = "menu_parametrage";  %>
<%! String sousMenuActuel = "menu_parametrage_annee";  %>


<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-cogs"></i> Paramétrage <span>Annee</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="/parametrage/annee" />">Paramétrage</a></li>
          <li class="active">Année</li>
        </ol>
      </div>
    </div>
    
    
    <div class="contentpanel">
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title">Modifier</h3>
        </div>
        <f:form method="post" class="form-horizontal" action="update" modelAttribute="anneeForm">
        <f:hidden path="id" />
			
        <div class="panel-body">
        <c:if test="${success_annee != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_annee}
							</div>
						</c:if>
						
						<c:if test="${error_annee != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_annee}
							</div>
						</c:if>
						
						<div class="row">
							<div class="col-sm-6">
							<div class="form-group">
                  <label class="col-sm-4 control-label">Intitulé<span class="asterisk">*</span> :</label>
                  <div class="col-sm-8">
                    <f:input path="intitule" cssClass="form-control" size="20"/>
                    <f:errors path="intitule" cssClass="error"></f:errors>
                  </div>
                </div>
                
             <div class="form-group">
                  <label class="col-sm-4 control-label">Actived :</label>
                  <div class="col-sm-8">
                  
               
                  	<div class="checkbox block"><label title="Année en cours " data-placement="top" data-toggle="tooltip" class="tooltips"><f:radiobutton path="actived" value="true" /> Activer</label></div>
				    <div class="checkbox block"><label title="Année désactiver" data-placement="top" data-toggle="tooltip" class="tooltips"><f:radiobutton path="actived" value="false" /> Désactiver</label></div>
				
				
				
				
				
				<f:errors path="actived" cssClass="error"></f:errors>
                  </div>
                </div>
                </div>
						</div>
              
        </div>
         <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-12">
				  <button type="submit" class="btn btn-primary">Valider</button>&nbsp;
				  <button type="reset" class="btn btn-default">Annuler</button>
				</div>
			 </div>
		  </div><!-- panel-footer -->
		  </f:form>
     </div>
     </div>
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp">
	<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
</jsp:include>


<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});


  });
</script>

</body>
</html>