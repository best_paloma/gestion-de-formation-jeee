<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>

<jsp:include page="../../views/layout/header.jsp">
	<jsp:param name="stylesheets" value="/assets/css/strength.css" />
</jsp:include>
<jsp:include page="../../views/layout/leftpanel.jsp" />
<jsp:include page="../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-users"></i> Compte <span>Paramétrage</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="#.">Compte</a></li>
          <li class="active">Paramétrage</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title">Modification mot de passe</h3>
        </div>
        
        
        
        
        
        <f:form method="post" class="form-horizontal" action="parametrage/update" modelAttribute="compteForm">
			
			<div class="panel-body">
			
			<c:if test="${success_user != null}">
			<div class="alert alert-success" role="alert">
				<strong>Well done!</strong> ${success_user}
			</div>
		</c:if>
		
		<c:if test="${error_user != null}">
			<div class="alert alert-danger" role="alert">
				<strong>Oh snap!</strong> ${error_user}
			</div>
		</c:if>
              
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="pw1">Ancien mot de passe:</label>
                  <div class="col-sm-4">
                <f:password path="password" cssClass="form-control" id="pw1" placeholder="Ancien mot de passe" required="true"  size="20"/>

                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-sm-3 control-label" for="pw2">Nouveau mot de passe:</label>
                  <div class="col-sm-4">
                  <div class="input-group mb15">
	                <span class="input-group-addon"><a href="#." class="button_strength" data-password-button="pw2"><i title="Afficher le mot de passe" class="fa fa-eye"></i></a></span>
	              	<f:password path="newPassword" cssClass="form-control" id="pw2" placeholder="Nouveau mot de passe" required="true"  size="20"/>
	              </div>
	              <div class="strength_meter"><div data-meter="pw2">Puissance</div></div>
                  </div>
                </div>
              </div><!-- panel-body -->
              <div class="panel-footer">
                <button type="submit" class="btn btn-primary">Valider</button>
            <button type="reset" class="btn btn-default">Annuler</button>
              </div><!-- panel-footer -->
		
        
	
		  </f:form>
     </div>
 </div>
<jsp:include page="../../views/layout/rightpanel.jsp" />
<jsp:include page="../../views/layout/footer.jsp">
	<jsp:param name="javascripts" value="/assets/js/strength.js" />
</jsp:include>

<script>
jQuery(document).ready(function($){
	$('#pw2').strength({
        strengthClass: 'strength',
        strengthMeterClass: 'strength_meter',
        strengthButtonClass: 'button_strength',
        strengthButtonText: 'Afficher le mot de passe',
        strengthButtonTextToggle: 'Cacher le mot de passe'
    });	
});
</script>


</body>
</html>