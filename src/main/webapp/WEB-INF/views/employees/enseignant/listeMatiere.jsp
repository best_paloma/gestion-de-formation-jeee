<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<%! String menuActuel = "menu_employer";  %>
<%! String sousMenuActuel = "menu_employer_professeurs";  %>

<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-users"></i> Employees <span>Enseignants</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="employees" />">Employees</a></li>
          <li class="active">Enseignants</li>
        </ol>
      </div>
    </div>
    
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
        </div>
        <div class="panel-body">
       
						
        	<div class="table-responsive">
	            <table class="table" id="table1">
	              <thead>
	                 <tr>
	                    <th>Matieres</th>
	                  
	                 </tr>
	              </thead>
	              <tbody>
	              
								
	                 
	              </tbody>
	             </table>
             </div>
        </div>
     </div>
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp" />

<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>

<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    
    jQuery('#table1').dataTable({
        "sPaginationType": "full_numbers"
      });
    
 // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });

  });
</script>

</body>
</html>