<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>


<%! String menuActuel = "menu_employer";  %>
<%! String sousMenuActuel = "menu_employer_professeurs";  %>


<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-users"></i> Employees <span>Enseignants</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="employees" />">Employees</a></li>
          <li class="active">Enseignants</li>
        </ol>
      </div>
    </div>
    
    <div class="contentpanel">
     <div class="panel panel-default">
        <div class="panel-heading">
          <div class="panel-btns">
            <a href="#" class="panel-close">&times;</a>
            <a href="#" class="minimize">&minus;</a>
          </div><!-- panel-btns -->
          <h3 class="panel-title">Ajouter</h3>
        </div>
        <f:form method="post" action="savePersonnel" modelAttribute="personnelForm">
			
        <div class="panel-body">
        <c:if test="${success_personnel != null}">
							<div class="alert alert-success" role="alert">
								<strong>Well done!</strong> ${success_personnel}
							</div>
						</c:if>
						
						<c:if test="${error_personnel != null}">
							<div class="alert alert-danger" role="alert">
								<strong>Oh snap!</strong> ${error_personnel}
							</div>
						</c:if>
			
						
        	 <div class="row mb15">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Prénom <span class="asterisk">*</span></label>
                    <f:input path="prenom" cssClass="form-control" size="20"/>
                    <f:errors path="prenom" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-6 -->
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nom <span class="asterisk">*</span></label>
                    <f:input path="nom" cssClass="form-control" size="20"/>
                    <f:errors path="nom" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-6 -->
              </div><!-- row -->
              <div class="row mb15">
              <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Compte Utilisateur</label>
                    <f:select path="utilisateur" class="form-control chosen-select">
                    	<f:option value="0"> --SELECTIONNER UN UTILISATEUR--</f:option>
					    <f:options items="${utilisateurs}" itemValue="id" itemLabel="username" />
					</f:select>
                    <f:errors path="utilisateur" cssClass="error"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Email</label>
                    <f:input path="email" cssClass="form-control" size="20"/>
                    <f:errors path="email" cssClass="errors"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Téléphone</label>
                    <f:input path="telephone" cssClass="form-control" size="20"/>
                    <f:errors path="telephone" cssClass="errors"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
              </div><!-- row -->
              <div class="row mb15">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Date entrée</label>
                    <f:input path="date_entree" cssClass="form-control datepicker-multiplee"/>
                    <f:errors path="date_entree" cssClass="errors"></f:errors>

                  </div>
                </div><!-- col-sm-6 -->
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Date sortie</label>
                     <f:input path="date_sortie" cssClass="form-control datepicker-multiplee"/>
                    <f:errors path="date_sortie" cssClass="errors"></f:errors>
                  </div>
                </div><!-- col-sm-6 -->
              </div><!-- row -->
              <div class="row mb15">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">CIN</label>
                    <f:input path="cin" cssClass="form-control" size="20"/>
                    <f:errors path="cin" cssClass="errors"></f:errors>
                  </div>
                </div><!-- col-sm-6 -->
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Sexe</label>
                    <f:select path="sexe" cssClass="form-control chosen-select">
					   <f:option value="0" label="--- Selectionnez ---"/>
					   <f:options items="${sexeList}" />
					</f:select>
                  </div>
                </div><!-- col-sm-6 -->
              </div><!-- row -->
              <div class="row">
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Banque</label>
                    <f:input path="banque" cssClass="form-control" size="20"/>
                    <f:errors path="banque" cssClass="errors"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Rib</label>
                    <f:input path="rib" cssClass="form-control" size="20"/>
                    <f:errors path="rib" cssClass="errors"></f:errors>
                  </div>
                </div><!-- col-sm-4 -->
                <div class="col-sm-4">
                  <div class="form-group">
                    <label class="control-label">Type Paiement</label>
                    <f:select path="type_paiement" cssClass="form-control chosen-select">
					   <f:option value="0" label="--- Selectionnez ---"/>
					   <f:options items="${paiementList}" />
					</f:select>
                    
                  </div>
                </div><!-- col-sm-4 -->
              </div><!-- row -->
              
        </div>
         <div class="panel-footer">
			 <div class="row">
				<div class="col-sm-12">
				  <button type="submit" class="btn btn-primary">Valider</button>&nbsp;
				  <button type="reset" class="btn btn-default">Annuler</button>
				</div>
			 </div>
		  </div><!-- panel-footer -->
		  </f:form>
     </div>
     </div>
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp">
	<jsp:param name="javascripts" value="/assets/js/chosen.jquery.min.js" />
</jsp:include>


<script>
  jQuery(document).ready(function() {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
    jQuery('.datepicker-multiplee').datepicker({
        numberOfMonths: 3,
        showButtonPanel: true
      });
    
    jQuery(".chosen-select").chosen({'width':'100%','white-space':'nowrap'});
 

  });
</script>

</body>
</html>