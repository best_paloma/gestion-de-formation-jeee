<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>
<%@taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>

<%! String menuActuel = "menu_notes";  %>
<%! String sousMenuActuel = "menu_notes_saisie";  %>

<jsp:include page="../../../views/layout/header.jsp" />
<jsp:include page="../../../views/layout/leftpanel.jsp" />
<jsp:include page="../../../views/layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-pencil"></i> Notes<span>Saisie de notes</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="controles" />">Notes</a></li>
          <li class="active">Saisie de notes</li>
        </ol>
      </div>
    </div>
    
 
 <div class="contentpanel">
 
 
       		<div class="pull-right">
       		<select id="controle_liste">
       		<option value="0">Veuillez selectionner un controle</option>
       		 <c:forEach var="v" items="${liste_controles}">
       		<option value="${v.id}">${v.intitule} - ${v.numControle }</option>
       		</c:forEach>
       		
       		</select>
       		
       		</div>
       		<div style="clear:both"></div>
       		<br>
        	<div class="table-responsive">
	            <table class="table" id="table1">
	              <thead>
	                 <tr>
	                
	                  
	                    <th width="50%">Etudiant</th>
	                    <th width="20%">Note</th>
	                    <th width="30%">Saisi</th>
	                
	                   
	                    
	                    
	                 </tr>
	              </thead>
	              <tbody id="resultat">
	            
							<tr><td colspan="3" class="text-center">Veuillez selectionner un controle</td></tr>
	                 
	              </tbody>
	             </table>
             </div>
            </div>
       
 
<jsp:include page="../../../views/layout/rightpanel.jsp" />
<jsp:include page="../../../views/layout/footer.jsp" />

<script src="<c:url value="/assets/js/chosen.jquery.min.js" />"></script>

<script>
  jQuery(document).ready(function($) {

    jQuery(".nav-parent > a#<%= menuActuel %>").trigger("click");
    jQuery(".nav-parent > a#<%= menuActuel %>").parent("li").addClass("active");
    jQuery(".nav-parent > ul.children > li#<%= sousMenuActuel %>").addClass("active");
    
 // Chosen Select
    jQuery("select").chosen({
      'min-width': '100px',
      'white-space': 'nowrap',
      disable_search_threshold: 10
    });
    
    function updateNote(ct_id,et_id,note){
    	$.post("<c:url value="/module/controle_note/set_note" />",{
			controle_id:ct_id,
			etudiant_id:et_id,
			note:note
		}, function(d){
			$("td#note_"+et_id).text(note);
			console.log(d);
		});
    }
    
    $('#controle_liste').change(function(){
    	
    	var controle_id=$(this).val();
    
    	
    	//console.log(controle_id);
    	$.get('<c:url value="/module/controle_note/get_etudiant" />',{
    		id:controle_id
    		
    		
    	},function(data){
    		if(data.length <= 0){
    			swal("Erreur","Aucun resultat trouver", "warning");
    			return;
    		}
    			
    		
    		console.log(data);
    		$('#resultat').empty();
    		$.each(data, function(cle,obj){
    			var ligne = '<tr>'+
    			'<td>'+obj.etudiant+'</td>'+
    			'<td id="note_'+obj.etudiant_id+'">'+obj.note+'</td>'+
    			'<td><div class="input-group"><input type="text" value="" data-id="'+obj.etudiant_id+'" class="form-control"><span class="input-group-btn"><input type="button" class="btn btn-success btn-note" data-controleid="'+controle_id+'" data-etudiantid="'+obj.etudiant_id+'" value="Valider" /></span></div></td>'+
    			'</tr>';
    			
    			
    			$('#resultat').append(ligne);
    		});
    		
    		
    		$(".btn-note").click(function(){
    			var ct_id = $(this).data("controleid");
    			var et_id = $(this).data("etudiantid");
    			
    			var note = $("input[data-id='"+et_id+"']").val();
    			
    			
    			
    			if(note.length < 0 || note == "" )
    				swal("Erreur","Veuillez entrez une note", "warning");
    			else if(note > -1 && note <= 20){
    				updateNote(ct_id,et_id,note);
    				
    			}
    			else{
    				swal(
      			 	      {
      			 	          title: "Êtes-vous sure?",
      			 	          text: "La note '"+note+"' est superieur a 20, souhaitez vous proceder quand meme ? ",
      			 	          type: "warning",
      			 	          showCancelButton: true,
      			 	          confirmButtonColor: "#DD6B55",
      			 	          confirmButtonText: "Oui, ajouter!",
      			 	          cancelButtonText: "Non, annuler!",
      			 	          closeOnConfirm: false,
      			 	          closeOnCancel: false
      			 	      },
      			 	      function(isConfirm)
      			 	      {
      			 	          if (isConfirm) {
      			 	        	updateNote(ct_id,et_id,note);
      			 	        	swal("Affectation!", "La note a ete bien affecter", "success");
      			 	          }
      			 	          else {
      			 	              swal("Annulé", "Aucune opération n'a été effectuer", "error");
      			 	          }
      			 	      });
    			}
    			
    			
    			
    			console.log(ct_id);
    			console.log(et_id);
    			console.log(note);
    			
    			
    			
    		});
    		
    	});
    	
    });
  

  });
</script>

</body>
</html>