package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Module;

public class ModuleService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Module> getAll() {
		List<Module> result = em.createQuery("SELECT p FROM Module p",
				Module.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Module p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Module p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Module update(Module p) throws NotFoundException{
		Module up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Module findById(int id) {
		return em.find(Module.class, id);
	}
}