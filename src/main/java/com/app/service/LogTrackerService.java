package com.app.service;


import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletRequest;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.core.userdetails.User;

import com.app.model.Logtracker;


public class LogTrackerService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Logtracker> getAll() {
		List<Logtracker> result = em.createQuery("SELECT p FROM Logtracker p",
				Logtracker.class).getResultList();
		return result;
	}
	@Transactional
	public List<Logtracker> getAll(int start, int end) {
		List<Logtracker> result = em.createQuery("SELECT p FROM Logtracker p",
				Logtracker.class).setFirstResult(start).setMaxResults(end).getResultList();
		return result;
	}
	@Transactional
	public List<Logtracker> getByDate(String startDate, String endDate) {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("MM/dd/yyyy");
		DateTime dtStartDate = formatter.parseDateTime(startDate);
		DateTime dtEndDate = formatter.parseDateTime(endDate);
		
		TypedQuery<Logtracker> e = em.createQuery("SELECT p FROM Logtracker p WHERE dateop between :start and :end", Logtracker.class);
		e.setParameter("start", dtStartDate);
		e.setParameter("end",dtEndDate);
		List<Logtracker> result = e.getResultList();
		return result;
	}

	@Transactional
	public void add(Logtracker p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Logtracker p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Logtracker update(Logtracker p) throws NotFoundException{
		Logtracker up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Logtracker findById(int id) {
		return em.find(Logtracker.class, id);
	}
	
	@Transactional
	public void store(String op, HttpServletRequest request){
		//System.out.println("Creating a new log entry");
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Logtracker l = new Logtracker();
		String userIpAddress = request.getRemoteAddr();
        String userAgent = request.getHeader("user-agent");
        String url = request.getRequestURL().toString() + "?" + request.getQueryString();
        
		l.setOperation(op);
		l.setUser(user.getUsername());
		l.setIp(userIpAddress);
		l.setUseragent(userAgent);
		l.setUrl(url);
		
		l.setDateop(new DateTime());
		//System.out.println(l);
		
		add(l);
	}
	
}