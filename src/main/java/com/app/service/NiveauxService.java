package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Niveaux;

public class NiveauxService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Niveaux> getAll() {
		List<Niveaux> result = em.createQuery("SELECT p FROM Niveaux p",
				Niveaux.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Niveaux p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Niveaux p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Niveaux update(Niveaux p) throws NotFoundException{
		Niveaux up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Niveaux findById(int id) {
		return em.find(Niveaux.class, id);
	}
}