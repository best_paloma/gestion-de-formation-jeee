package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Semestre;

public class SemestreService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Semestre> getAll() {
		List<Semestre> result = em.createQuery("SELECT p FROM Semestre p",
				Semestre.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Semestre p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Semestre p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Semestre update(Semestre p) throws NotFoundException{
		Semestre up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Semestre findById(int id) {
		return em.find(Semestre.class, id);
	}
}