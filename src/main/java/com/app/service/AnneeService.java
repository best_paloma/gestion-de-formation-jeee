package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Annee;


public class AnneeService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Annee> getAll() {
		List<Annee> result = em.createQuery("SELECT p FROM Annee p",
				Annee.class).getResultList();
		return result;
	}
	@Transactional
	public List<Annee> getActive() {
		List<Annee> result = em.createQuery("SELECT p FROM Annee p where  actived=true",
				Annee.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Annee p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Annee p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Annee update(Annee p) throws NotFoundException{
		Annee up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Annee findById(int id) {
		return em.find(Annee.class, id);
	}
}