package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import com.app.enums.Controlenum;
import com.app.enums.TypeControle;
@Entity
@Table(name = "controle_note")
public class Controle_note {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_controle_note")
	private int id;
	@ManyToOne
	@JoinColumn(name="id_controle")
	private Controle controle;
	@ManyToOne
	@JoinColumn(name="id_etudiant")
	private Etudiant etudiant;
	@ManyToOne
	@JoinColumn(name="id_annee")
	private Annee annee;;

	private String decision;
	private Double note;


	public Controle_note(int id, Controle controle, Etudiant etudiant,
			Annee annee, String decision, Double note) {
		super();
		this.id = id;
		this.controle = controle;
		this.etudiant = etudiant;
		this.annee = annee;
		this.decision = decision;
		this.note = note;
		
	}
	public Controle_note() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Controle getControle() {
		return controle;
	}
	public void setControle(Controle controle) {
		this.controle = controle;
	}
	public Etudiant getEtudiant() {
		return etudiant;
	}
	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	public Annee getAnnee() {
		return annee;
	}
	public void setAnnee(Annee annee) {
		this.annee = annee;
	}
	public String getDecision() {
		return decision;
	}
	public void setDecision(String decision) {
		this.decision = decision;
	}
	public Double getNote() {
		return note;
	}
	public void setNote(Double note) {
		this.note = note;
	}
	


	
	
}
