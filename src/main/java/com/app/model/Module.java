package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "module")
public class Module {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_module")
	private int id;
	@NotEmpty(message = "Veuillez entrer un nom.")
	@Size(min = 4, max = 120, message = "Le nom doit etre compris entre 4 et 120 caracteres")
	private String intitule;
	@ManyToOne
	@JoinColumn(name="id_centre")
	private Centre centre;
	@ManyToOne
	@JoinColumn(name="id_annee")
	private Annee annee;
	@ManyToOne
	@JoinColumn(name="id_formation")
	private Formation formation;
	@ManyToOne
	@JoinColumn(name="id_filiere")
	private Filiere filiere;


	@ManyToOne
	@JoinColumn(name="id_niveau")
	private Niveaux niveau;
	@ManyToOne
	@JoinColumn(name="id_semestre")
	private Semestre semestre;
	public String getContextuel() {
		return contextuel;
	}


	public void setContextuel(String contextuel) {
		this.contextuel = contextuel;
	}
private String contextuel;

	public Semestre getSemestre() {
		return semestre;
	}


	public void setSemestre(Semestre semestre) {
		this.semestre = semestre;
	}


	public Niveaux getNiveau() {
		return niveau;
	}


	public void setNiveau(Niveaux niveau) {
		this.niveau = niveau;
	}


	private int coefficient;
	public Centre getCentre() {
		return centre;
	}


	public void setCentre(Centre centre) {
		this.centre = centre;
	}


	public Annee getAnnee() {
		return annee;
	}


	public void setAnnee(Annee annee) {
		this.annee = annee;
	}


	public Formation getFormation() {
		return formation;
	}


	public void setFormation(Formation formation) {
		this.formation = formation;
	}


	public Filiere getFiliere() {
		return filiere;
	}


	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}


	public int getCoefficient() {
		return coefficient;
	}


	public void setCoefficient(int coefficient) {
		this.coefficient = coefficient;
	}


	public Module() {
		super();
	}


	public Module(int id, String intitule) {
		super();
		this.id = id;
		this.intitule = intitule;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getIntitule() {
		return intitule;
	}


	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}


	@Override
	public String toString() {
		return intitule;
	}


	
	
	
	
	

}
