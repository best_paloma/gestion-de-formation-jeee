package com.app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.joda.time.DateTime;

@Entity
@DiscriminatorValue("diplome")
public class Diplome extends Document{
	@OneToMany(mappedBy = "diplome", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@OrderBy("field ASC")
    private Set<DiplomeField> fields = new HashSet<DiplomeField>();

	public Diplome() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public Diplome(int id, String intitule, String path,
			DateTime date_creation, Set<DiplomeField> fields) {
		super(id, intitule, path, date_creation);
		this.fields = fields;
	}



	public Diplome(int id, String intitule, String path, DateTime date_creation) {
		super(id, intitule, path, date_creation);
		// TODO Auto-generated constructor stub
	}



	public Set<DiplomeField> getFields() {
		return fields;
	}

	public void setFields(Set<DiplomeField> fields) {
		this.fields = fields;
	}
	
	public void addField(DiplomeField field) {
		  if (fields == null) {
		    fields = new HashSet<DiplomeField>();
		  }
		  fields.add(field);
		  field.setDiplome(this);
	}
	
	
}
