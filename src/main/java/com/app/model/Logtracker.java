package com.app.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "JournalLog")
public class Logtracker {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String user;
	@Type(type="text")
	private String operation;
	private String ip;
	@Type(type="text")
	private String useragent;

	@Column
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	private DateTime dateop;
	
	private String url;
	
	public Logtracker(){}

	

	public Logtracker(int id, String user,  String operation,
			String ip, String useragent, DateTime dateop) {
		super();
		this.id = id;
		this.user = user;
		this.operation = operation;
		this.ip = ip;
		this.useragent = useragent;
		this.dateop = dateop;
	}


	

	public String getUrl() {
		return url;
	}



	public void setUrl(String url) {
		this.url = url;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	

	public String getUser() {
		return user;
	}



	public void setUser(String user) {
		this.user = user;
	}


	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUseragent() {
		return useragent;
	}

	public void setUseragent(String useragent) {
		this.useragent = useragent;
	}

	public DateTime getDateop() {
		return dateop;
	}

	public void setDateop(DateTime dateop) {
		this.dateop = dateop;
	}

	@Override
	public String toString() {
		return "Logtracker [id=" + id + ", user=" + user + ", operation="
				+ operation + ", ip=" + ip + ", useragent=" + useragent
				+ ", dateop=" + dateop + "]";
	}
	
	
	
}
