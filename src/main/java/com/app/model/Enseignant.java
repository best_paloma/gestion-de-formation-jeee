package com.app.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import org.joda.time.DateTime;

@Entity
@DiscriminatorValue("enseignant")
public class Enseignant extends Person{

	private String diplome;
	
	

	public Enseignant() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Enseignant(int id, String prenom, String nom, String sexe,
			String cin, String telephone, String email, DateTime date_entree,
			DateTime date_sortie, String banque, String rib, int type_paiement, String diplome) {
		super(id, prenom, nom, sexe, cin, telephone, email, date_entree, date_sortie,
				banque, rib, type_paiement);
		this.diplome = diplome;
	}

	public String getDiplome() {
		return diplome;
	}

	public void setDiplome(String diplome) {
		this.diplome = diplome;
	}

	@Override
	public String toString() {
		return this.getFullname();
	}

	
	
	
	
}
