package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import com.app.enums.PaiementType;

@Entity
@Table(name = "etudiant")
public class Etudiant {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_etudiant")
	private int id;
	private String nom;
	private String prenom;
	
	@Column
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private DateTime date;
	private String CIN;
	private String lieu;
	private String telephone;
	private String email;
	private String fullName;
	private int reduction;
	
	@ManyToOne
	@JoinColumn(name="id_annee")
	private Annee annee;
	
	@ManyToOne
	@JoinColumn(name="id_centre")
	private Centre centre;
	
	@ManyToOne
	@JoinColumn(name="id_formation")
	private Formation formation;
	
	@ManyToOne
	@JoinColumn(name="id_filiere")
	private Filiere filiere;
	
	@ManyToOne
	@JoinColumn(name="id_niveaux")
	private Niveaux niveaux;
	
	@ManyToOne
	@JoinColumn(name="id_user")
	private User utilisateur;
	
	@Column(name="type")
	@Enumerated(EnumType.ORDINAL)
	private PaiementType type;
	
	
	public Etudiant() {
		super();
	}



	
	
	public Etudiant(int id, String nom, String prenom, DateTime date,
			String cIN, String lieu, String telephone, String email,
			String fullName, Annee annee, Centre centre, Formation formation,
			Filiere filiere, Niveaux niveaux, User utilisateur,
			PaiementType type, int reduction) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.date = date;
		CIN = cIN;
		this.lieu = lieu;
		this.telephone = telephone;
		this.email = email;
		this.fullName = fullName;
		this.annee = annee;
		this.centre = centre;
		this.formation = formation;
		this.filiere = filiere;
		this.niveaux = niveaux;
		this.utilisateur = utilisateur;
		this.type = type;
		this.reduction = reduction;
	}





	public int getReduction() {
		return reduction;
	}

	public void setReduction(int reduction) {
		this.reduction = reduction;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public DateTime getDate() {
		return date;
	}

	public void setDate(DateTime date) {
		this.date = date;
	}

	public String getCIN() {
		return CIN;
	}

	public void setCIN(String cIN) {
		CIN = cIN;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFullName() {
		return nom + " " +prenom;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Annee getAnnee() {
		return annee;
	}

	public void setAnnee(Annee annee) {
		this.annee = annee;
	}

	public Centre getCentre() {
		return centre;
	}

	public void setCentre(Centre centre) {
		this.centre = centre;
	}

	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

	public Niveaux getNiveaux() {
		return niveaux;
	}

	public void setNiveaux(Niveaux niveaux) {
		this.niveaux = niveaux;
	}
	
	

	public User getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(User utilisateur) {
		this.utilisateur = utilisateur;
	}

	public PaiementType getType() {
		return type;
	}

	public void setType(PaiementType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return nom;
	}


	
	
	
}