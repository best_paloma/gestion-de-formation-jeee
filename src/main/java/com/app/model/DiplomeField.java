package com.app.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "documents_diplome_fields")
public class DiplomeField implements Comparable<DiplomeField>{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_field")
	private int id;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Diplome diplome;
	
	private String field;
	
	private String value;

	public DiplomeField() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DiplomeField(int id, Diplome diplome, String field, String value) {
		super();
		this.id = id;
		this.diplome = diplome;
		this.field = field;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Diplome getDiplome() {
		return diplome;
	}

	public void setDiplome(Diplome diplome) {
		this.diplome = diplome;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public int compareTo(DiplomeField o) {
		return o.getId() - this.id;
	}
	
	
	
	
}
