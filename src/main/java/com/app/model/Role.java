package com.app.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Role {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@NotEmpty(message = "Veuillez entrer un nom.")
	private String name;
	
	@ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "role_permission", joinColumns = @JoinColumn(name = "role_id"), inverseJoinColumns = @JoinColumn(name = "id_permission"))
	private Collection<Permission> permissions = new ArrayList<Permission>();
	
	
	public Role() {
		super();
	}
	
	
	public Role(int id, String name, Collection<Permission> permissions) {
		super();
		this.id = id;
		this.name = name;
		this.permissions = permissions;
	}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}



	public Collection<Permission> getPermissions() {
		return permissions;
	}



	public void setPermissions(Collection<Permission> permissions) {
		this.permissions = permissions;
	}

	
	@Override
    public int hashCode() {
        return new Integer(id).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (! (obj instanceof Role)) {
            return false;
        }
        return this.id == ((Role)obj).getId();
    }

/*
	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", permissions="
				+ permissions + "]";
	}*/
	
	
	
	
	
}
