package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.enums.Controlenum;
import com.app.enums.PaiementType;
import com.app.enums.TypeControle;



public class ControleNumEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
    @Override
    public void setAsText(String text) {
        this.setValue(Controlenum.valueOf(text));
    }

    // Converts a Permission to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	Controlenum c = (Controlenum) this.getValue();
    	if (c != null)
            return c.name();
        return null;
    }

}
