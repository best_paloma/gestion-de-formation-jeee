package com.app.documents;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import com.app.model.Logtracker;

public class LogListExcelView extends AbstractExcelView{
	@Override
	protected void buildExcelDocument(Map model, HSSFWorkbook workbook,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception
	
	{
		response.setHeader("Content-Disposition", "attachment; filename=\"journal.xls\"");

		HSSFSheet excelSheet = workbook.createSheet("Journeaux de logs");
		setExcelHeader(excelSheet);
		
		List logList = (List) model.get("logsList");
		setExcelRows(excelSheet,logList);
		
	}

	public void setExcelHeader(HSSFSheet excelSheet) {
		HSSFRow excelHeader = excelSheet.createRow(0);
		excelHeader.createCell(0).setCellValue("Id");
		excelHeader.createCell(1).setCellValue("User");
		excelHeader.createCell(2).setCellValue("Date");
		excelHeader.createCell(3).setCellValue("Operation");
		excelHeader.createCell(4).setCellValue("IP");
		excelHeader.createCell(5).setCellValue("URL");
		excelHeader.createCell(6).setCellValue("UserAgent");
	}
	
	public void setExcelRows(HSSFSheet excelSheet, List<Logtracker> logsList){
		int record = 1;
		for (Logtracker log : logsList) {
			HSSFRow excelRow = excelSheet.createRow(record++);
			excelRow.createCell(0).setCellValue(log.getId());
			excelRow.createCell(1).setCellValue(log.getUser());
			excelRow.createCell(2).setCellValue(log.getDateop().toString());
			excelRow.createCell(3).setCellValue(log.getOperation());
			excelRow.createCell(4).setCellValue(log.getIp());
			excelRow.createCell(5).setCellValue(log.getUrl());
			excelRow.createCell(6).setCellValue(log.getUseragent());
		}
	}
}
