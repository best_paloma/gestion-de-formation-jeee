package com.app.controller;

import java.util.HashSet;

import java.util.Set;

import javassist.NotFoundException;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.model.Message;
import com.app.model.MessageThread;
import com.app.model.User;
import com.app.propertyeditors.UserEditor;
import com.app.service.MessageService;
import com.app.service.UserService;

@Controller
public class MessagerieController {
	@Autowired
	MessageService messageService;
	@Autowired
	UserService userService;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(User.class, new UserEditor());
    }
	
	
	@RequestMapping(value = "/messagerie", method = RequestMethod.GET)
	public String index(Model model) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
		User u;
		try {
			u = userService.findByUsername(userDetails.getUsername());
			model.addAttribute("inbox_messages", messageService.getInbox(u));
			model.addAttribute("user_id", u.getId());
			return "messagerie/inbox";
		} catch (NotFoundException e) {
			return "403";
		}
	}
	
	@RequestMapping(value = "/messagerie/outbox", method = RequestMethod.GET)
	public String outbox(Model model) {
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
		User u;
		try {
			u = userService.findByUsername(userDetails.getUsername());
			model.addAttribute("inbox_messages", messageService.getOutbox(u));
			model.addAttribute("user_id", u.getId());
			return "messagerie/outbox";
		} catch (NotFoundException e) {
			return "403";
		}
	}
	
	
	@RequestMapping(value = "/messagerie/view", method = RequestMethod.GET)
	public String read(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model) {
		MessageThread mt =  messageService.findById(id);
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
		User u;
		try {
			u = userService.findByUsername(userDetails.getUsername());
			Set<Message> l = (Set<Message>) mt.getMessages();
			int uid = u.getId();
			
			for(Message m : l)
			{
				
				
				if(uid == m.getTo().getId()){
					m.setRead(1); // mark as read
					System.out.println("mark message as readed");
					//messageService.updateChild(m);
				}
				//m.setMessageThread(null);
				 
				
				//System.out.println(m);
					
				//n.add(m);
			}
		    //mt.setMessages(new HashSet<Message>());
			//mt.setDateMessage(new DateTime());
			messageService.update(mt);
		} catch (NotFoundException e1) {
			System.out.println(e1.getMessage());
		}
		
		
		
		
		MessageThread mt2 =  messageService.findById(id);
		
		/*if(mt.getTo().getId() == u.getId()){
			mt.setRead(true);
			messageService.update(mt);
		}*/
		model.addAttribute("message", mt2);
		return "messagerie/read";
	}
	
	@RequestMapping(value = "/messagerie/reply", method = RequestMethod.POST)
	public String reply(
			@RequestParam(value = "thread_id", required = true, defaultValue = "0") int id,
			@RequestParam(value = "message", required = true, defaultValue = "") String contenu,
			Model model) throws NotFoundException {
		System.out.println("INSERT NEW REPLY");
		System.out.println(id);
		System.out.println(contenu);
		MessageThread mt =  messageService.findById(id);
		mt.setRead(false);
		Message m = new Message();
		m.setRead(2); // not read
		m.setContenu(contenu);
		m.setDateMessage(new DateTime());
		m.setMessageThread(mt);
		
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
		User u = userService.findByUsername(userDetails.getUsername());
		m.setFrom(u);
		
		if(u.getId() == mt.getFrom().getId())
			m.setTo(mt.getTo());
		else if(u.getId() == mt.getTo().getId())
			m.setTo(mt.getFrom());
		
		mt.addMessage(m);
		
		messageService.update(mt);
		
		return "redirect:/messagerie/view?id="+id;
	}
	
	
	@RequestMapping(value = "/messagerie/compose", method = RequestMethod.GET)
	public String compose(
			Model model) {
		model.addAttribute("users", userService.getAll());
		return "messagerie/compose";
	}
	
	@RequestMapping(value = "/messagerie/envoyer", method = RequestMethod.POST)
	public String envoyer(
			@RequestParam(value = "subject", required = true, defaultValue = "") String subject,
			@RequestParam(value = "user", required = true, defaultValue = "0") int user_id,
			@RequestParam(value = "message", required = true, defaultValue = "") String contenu,
			Model model) throws NotFoundException {
		
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
		User u = userService.findByUsername(userDetails.getUsername());
		
		User to = userService.findById(user_id);
		
		MessageThread msg1 = new MessageThread();
		msg1.setSubject(subject);
		msg1.setDateMessage(new DateTime());
		msg1.setFrom(u);
		msg1.setTo(to);
		msg1.setRead(false);
		
		Message cmsg1 = new Message();
		cmsg1.setFrom(u);
		cmsg1.setTo(to);
		cmsg1.setDateMessage(new DateTime());
		cmsg1.setMessageThread(msg1);
		cmsg1.setContenu(contenu);
		
		msg1.addMessage(cmsg1);
		
		messageService.add(msg1);
		
		model.addAttribute("success", true);
		return "messagerie/compose";
	}
	
}
