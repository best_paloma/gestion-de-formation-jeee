package com.app.controller;

import javassist.NotFoundException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.form.Compte;
import com.app.model.User;
import com.app.service.UserService;

@Controller
public class CompteController {
	@Autowired
	UserService userService;
	
	
	@RequestMapping(value = "/compte/parametrage", method = RequestMethod.GET)
	public String index(Model model) {
		Compte compte = new Compte();
		//compte.setPassword("123456");
		//compte.setNewPassword("123456789");
		model.addAttribute("compteForm", compte);
		return "compte/parametrage";
	}
	
	@RequestMapping(value = "/compte/parametrage/update", method = RequestMethod.POST)
	public String update(
			@Valid @ModelAttribute("compteForm") Compte p,
			Model model,
			final RedirectAttributes redirectAttributes
			) throws Exception {
		
		// Hash old password to compare to current user password
		Md5PasswordEncoder encoder = new Md5PasswordEncoder();
	    String hashedPass = encoder.encodePassword(p.getPassword(), null);
	    
	    UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
	    
	    try {
			User u = userService.findByUsername(userDetails.getUsername());
			
			if(u.getPassword().equals(hashedPass))
			{

			    String newHashedPass = encoder.encodePassword(p.getNewPassword(), null);
			    u.setPassword(newHashedPass);
			    userService.update(u);
			    redirectAttributes.addFlashAttribute("success_user", "Mise à jour du mot de passe avec succés");
				return "redirect:/compte/parametrage";
			    
			}
			else
			{
				redirectAttributes.addFlashAttribute("error_user", "L'ancien mot de passe ne correspond pas");
				return "redirect:/compte/parametrage";
			}
			
			
			
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_user", "Échec de la mise à jour du mot de passe");
			return "redirect:/compte/parametrage";
		}
		
		
	}
	
}
