package com.app.controller;



import java.util.ArrayList;
import java.util.List;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.enums.Controlenum;
import com.app.json.JEtudiantNote;
import com.app.json.JResponse;
import com.app.model.Annee;
import com.app.model.Controle;
import com.app.model.Controle_note;
import com.app.model.Etudiant;
import com.app.model.Matiere;
import com.app.propertyeditors.AnneeEditor;
import com.app.propertyeditors.ControleEditor;
import com.app.propertyeditors.ControleNumEditor;
import com.app.propertyeditors.EtudiantEditor;
import com.app.service.AnneeService;
import com.app.service.ControleService;
import com.app.service.Controle_noteService;
import com.app.service.EtudiantService;
import com.app.service.LogTrackerService;
import com.app.service.MatiereService;

@Controller
public class Controle_noteController {
	
	@Autowired
	MatiereService matiereSvc;
	@Autowired
	AnneeService anneeSvc;

	@Autowired
	ControleService controleSvc;
	@Autowired
	EtudiantService etudiantSvc;
	@Autowired
	Controle_noteService controle_noteSvc;
	@Autowired
	LogTrackerService logSvc;
	@InitBinder
    public void initBinder(WebDataBinder binder) {
      
        binder.registerCustomEditor(Annee.class, new AnneeEditor());
       
        binder.registerCustomEditor(Controle.class, new ControleEditor());
   
        binder.registerCustomEditor(Etudiant.class, new EtudiantEditor());
     
       
    }
	
	
	@RequestMapping(value = "/module/controle_note/get_etudiant", method = RequestMethod.GET)
	public @ResponseBody List<JEtudiantNote> get_etudiant(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id) 
	{
		 List<JEtudiantNote> list = new  ArrayList<JEtudiantNote>();
		Controle ct=new Controle();
		ct.setId(id);
		try {
			List<Etudiant> et = etudiantSvc.findByControle(ct);
			
			if(et == null)
				return list;
			
			for(int i=0;i<et.size();i++){
				Etudiant etudiant = et.get(i);
				Controle_note bnote = controle_noteSvc.findByControleEtudiant(ct, etudiant);
				Double note = (bnote == null) ? 0.0 : bnote.getNote();
				
				
				list.add(new JEtudiantNote(etudiant.getFullName(), etudiant.getId(), note));
			}
			
			return list;
		} catch (NotFoundException e) {
			return list;
		}

	}
	
	@RequestMapping(value = "/module/controle_note/set_note", method = RequestMethod.POST)
	public @ResponseBody JResponse set_note(
			@RequestParam(value = "controle_id", required = true, defaultValue = "0") int ct_id,
			@RequestParam(value = "etudiant_id", required = true, defaultValue = "0") int et_id,
			@RequestParam(value = "note", required = true, defaultValue = "0") Double note
	) 
	{
		 
		Etudiant e = new Etudiant();
		e.setId(et_id);

		Controle c = new Controle();
		c.setId(ct_id);
		
		try {
			if(controle_noteSvc.findByControleEtudiant(c, e) == null) //ajout
			{ 
				System.out.println("Insert");
				Controle_note cn=new Controle_note();
				cn.setEtudiant(e);
				cn.setControle(c);
				cn.setAnnee(controleSvc.findById(ct_id).getAnnee());
				
				cn.setDecision("");
				cn.setNote(note);
				controle_noteSvc.add(cn);
			}
			else //update
			{
				System.out.println("Update");
				Controle_note cn=controle_noteSvc.findByControleEtudiant(c, e);
				cn.setNote(note);
				controle_noteSvc.update(cn);
			}
			
			return new JResponse("200", "nice work");
		} catch (NotFoundException e1) {
			return new JResponse("504", e1.getMessage());
		}
	}
	
	
	@PreAuthorize("hasRole('CONTROLE_READ')")
	@RequestMapping(value = "/module/controle_note", method = RequestMethod.GET)
	public String index(Model model) 
	{
		//model.addAttribute("mode", "create");
		
		
		model.addAttribute("liste_matiere", matiereSvc.getAll());
		model.addAttribute("liste_etudiant", etudiantSvc.getAll());
		
		model.addAttribute("liste_annee", anneeSvc.getAll());
		
	model.addAttribute("controle_noteForm", new Controle_note());
		model.addAttribute("liste_controles", controleSvc.getAll());
		model.addAttribute("liste_note", controle_noteSvc.getAll());
		return "/module/controle_note/liste";
	}
	
	
	@PreAuthorize("hasRole('CONTROLE_EDIT')")
	@RequestMapping(value = "/module/controle_note/ajouter", method = RequestMethod.GET)
	public String ajouter(Model model) 
	{
	
		model.addAttribute("controle_noteForm", new Controle_note());
		model.addAttribute("m", new Matiere());
		model.addAttribute("e", new Etudiant());
		model.addAttribute("liste_matiere", matiereSvc.getAll());
		model.addAttribute("liste_etudiant", etudiantSvc.getAll());
		model.addAttribute("controle_num", Controlenum.values());
		model.addAttribute("liste_annee", anneeSvc.getActive());
		

		model.addAttribute("liste_controles", controleSvc.findbyYear(anneeSvc.getActive().get(0)));

	
	
		
	
		return "module/controle_note/ajouter";
	}
	
	@PreAuthorize("hasRole('CONTROLE_EDIT')")
	@RequestMapping(value = "/module/controle_note/saveControle_note", method = RequestMethod.POST)
	public String saveControle_note(
			@Valid @ModelAttribute("controle_noteForm") Controle_note v,
			BindingResult bindingResult,
			@RequestParam(value = "mode", required = true, defaultValue = "create") String mode,
			Model model,
			final RedirectAttributes redirectAttributes,HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("controle_noteForm", v);
			model.addAttribute("m", new Matiere());
			model.addAttribute("e", new Etudiant());
			model.addAttribute("liste_matiere", matiereSvc.getAll());
			model.addAttribute("liste_etudiant", etudiantSvc.getAll());
			model.addAttribute("controle_num", Controlenum.values());
			
			model.addAttribute("liste_annee", anneeSvc.getActive());
			
		
			model.addAttribute("liste_controles", controleSvc.findbyYear(anneeSvc.getActive().get(0)));
			model.addAttribute("liste_note", controle_noteSvc.getAll());
			return "module/controle_note/ajouter";
		} else {
			
			if(mode.equals("create"))
			{
				controle_noteSvc.add(v);
				logSvc.store("Ajout la note du Controle: "+v, request);
				redirectAttributes.addFlashAttribute("success_controle_note", v.getControle().getIntitule() + " a ete bien ajouter");
				return "redirect:/module/controle_note/ajouter";
			}
			else{
				System.out.println("Updating "+v);
				try {
				controle_noteSvc.update(v);
					redirectAttributes.addFlashAttribute("success_controle_note", v.getControle().getIntitule() + " a ete bien modifier");
					return "redirect:/module/controle_note/ajouter";
				} catch (NotFoundException e) {
					redirectAttributes.addFlashAttribute("error_controle_note", "Entity not found");
					return "redirect:/module/controle_note/ajouter";
				}
				
			}
			
		}
	}
	
	@PreAuthorize("hasRole('CONTROLE_READ')")
	@RequestMapping(value = "/module/controle_note/editNote", method = RequestMethod.GET)
	public String editModule(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Controle_note p = controle_noteSvc.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("controle_noteForm", p);
		model.addAttribute("liste_matiere", matiereSvc.getAll());
		model.addAttribute("liste_etudiant", etudiantSvc.getAll());
		model.addAttribute("controle_num", Controlenum.values());
		model.addAttribute("liste_annee", anneeSvc.getActive());
		
	
		model.addAttribute("liste_controles", controleSvc.findbyYear(anneeSvc.getActive().get(0)));
		
		return "module/controle_note/modifier";
	}

	@PreAuthorize("hasRole('CONTROLE_EDIT')")
	@RequestMapping(value = "/module/controle_note/updatenote", method = RequestMethod.POST)
	public  String updatemodule(
			@Valid @ModelAttribute("controle_noteForm") Controle_note p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("controle_noteForm", p);
			
			model.addAttribute("liste_matiere", matiereSvc.getAll());
			
			model.addAttribute("liste_etudiant", etudiantSvc.getAll());
			
			model.addAttribute("liste_annee", anneeSvc.getActive());
			model.addAttribute("controle_num", Controlenum.values());
		
			model.addAttribute("liste_controles", controleSvc.findbyYear(anneeSvc.getActive().get(0)));
		
		
			
			return "modules/controle_note";
		} else {
			
			try {
				controle_noteSvc.update(p);
				logSvc.store("Modification la note du Controle: "+p, request);
				redirectAttributes.addFlashAttribute("success_controle_note", p.getControle().getIntitule() + " a ete bien modifier");
				return "redirect:/module/controle_note/editNote?id="+p.getId();
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error_controle_note", "Entity not found");
				return "redirect:/module/controle_note/editNote?id="+p.getId();
			}
			
		}
	}
	
	@PreAuthorize("hasRole('CONTROLE_EDIT')")
	@RequestMapping(value = "/module/controle_note/deleteNote", method = RequestMethod.GET)
	public String deleteModule(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Controle_note p = controle_noteSvc.findById(id);
			controle_noteSvc.delete(id);
			logSvc.store("Suppression de la note: "+p, request);

			redirectAttributes.addFlashAttribute("success_controle_note_delete", p.getControle().getIntitule()
					+ " a ete supprimer");

			return "redirect:/module/controle_note";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_controle_note_delete", "Entity not found");
			return "redirect:/module/controle_note";
		}
		
	}
	

}
