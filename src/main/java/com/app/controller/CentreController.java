package com.app.controller;

import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.Ville;
import com.app.service.LogTrackerService;
import com.app.service.VilleService;
import com.app.model.Centre;

import com.app.propertyeditors.VilleEditor;
import com.app.service.CentreService;

@Controller
public class CentreController {
	@Autowired
	VilleService villeSvc;
	@Autowired
	CentreService centreSvc;
	@Autowired
	LogTrackerService logSvc;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Ville.class, new VilleEditor());
      

    }
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/centres", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("mode", "create");
		model.addAttribute("villeForm", new Ville());
		model.addAttribute("liste_villes", villeSvc.getAll());
		
		model.addAttribute("centreForm", new Centre());
		model.addAttribute("liste_centre", centreSvc.getAll());
		
		return "parametrage/centre";
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/saveVille", method = RequestMethod.POST)
	public String savePerson(
			@Valid @ModelAttribute("villeForm") Ville v,
			BindingResult bindingResult,
			@RequestParam(value = "mode", required = true, defaultValue = "create") String mode,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			
			model.addAttribute("villeForm", new Ville());
			model.addAttribute("liste_villes", villeSvc.getAll());
			
			model.addAttribute("centreForm", new Centre());
			model.addAttribute("liste_centre", centreSvc.getAll());
			return "parametrage/centre";
		} else {
			
			if(mode.equals("create"))
			{
				villeSvc.add(v);
				logSvc.store("Ajout Ville: "+v, request);
				redirectAttributes.addFlashAttribute("success_ville", v.getIntitule() + " a ete bien ajouter");
				return "redirect:/parametrage/centres";
			}
			else{
				System.out.println("Updating "+v);
				try {
					villeSvc.update(v);
					logSvc.store("Mise a jour ville: "+v, request);
					redirectAttributes.addFlashAttribute("success_ville", v.getIntitule() + " a ete bien modifier");
					return "redirect:/parametrage/centres";
				} catch (NotFoundException e) {
					redirectAttributes.addFlashAttribute("error_ville", "Entity not found");
					return "redirect:/parametrage/centres";
				}
				
			}
			
		}
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/saveCentre", method = RequestMethod.POST)
	public String saveCentre(
			@Valid @ModelAttribute("centreForm") Centre v,
			BindingResult bindingResult,
			@RequestParam(value = "mode", required = true, defaultValue = "create") String mode,
			//@RequestParam(value = "ville_id", required = true, defaultValue = "0") int id_ville,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			model.addAttribute("villeForm", new Ville());
			model.addAttribute("liste_villes", villeSvc.getAll());
			
			model.addAttribute("centreForm", new Centre());
			model.addAttribute("liste_centre", centreSvc.getAll());
			return "parametrage/centre";
		} else {
			
			//Ville p = villeSvc.findById(id_ville);
			//v.setVille(p);
			
			if(mode.equals("create"))
			{
				centreSvc.add(v);
				logSvc.store("Ajout Centre: "+v, request);
				redirectAttributes.addFlashAttribute("success_centre", v.getIntitule() + " a ete bien ajouter");
				return "redirect:/parametrage/centres";
			}
			else{
				System.out.println("Updating "+v);
				try {
					centreSvc.update(v);
					logSvc.store("Mise a jour Centre: "+v, request);
					redirectAttributes.addFlashAttribute("success_centre", v.getIntitule() + " a ete bien modifier");
					return "redirect:/parametrage/centres";
				} catch (NotFoundException e) {
					redirectAttributes.addFlashAttribute("error_centre", "Entity not found");
					return "redirect:/parametrage/centres";
				}
				
			}
			
		}
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/editVille", method = RequestMethod.GET)
	public String editPerson(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Ville p = villeSvc.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("mode", "edit");
		model.addAttribute("villeForm", p);
		model.addAttribute("centreForm", new Centre());
		
		
		model.addAttribute("liste_villes", villeSvc.getAll());
		model.addAttribute("liste_centre", centreSvc.getAll());
		return "parametrage/centre";
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/editCentre", method = RequestMethod.GET)
	public String editCentre(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Centre p = centreSvc.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("mode", "edit");
		model.addAttribute("centreForm", p);
		model.addAttribute("villeForm", new Ville());
		
		model.addAttribute("liste_villes", villeSvc.getAll());
		model.addAttribute("liste_centre", centreSvc.getAll());
		
		return "parametrage/centre";
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/deleteVille", method = RequestMethod.GET)
	public String deleteVille(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Ville p = villeSvc.findById(id);
			villeSvc.delete(id);
			logSvc.store("Suppression ville: "+p, request);

			redirectAttributes.addFlashAttribute("success_ville_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/parametrage/centres";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_ville_delete", "Entity not found");
			return "redirect:/parametrage/centres";
		}
		
	}
	
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/parametrage/deleteCentre", method = RequestMethod.GET)
	public String deleteCentre(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Centre p = centreSvc.findById(id);
			centreSvc.delete(id);
			logSvc.store("Suppression Centre: "+p, request);

			redirectAttributes.addFlashAttribute("success_centre_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/parametrage/centres";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_centre_delete", "Entity not found");
			return "redirect:/parametrage/centres";
		}
		
	}
	
}
