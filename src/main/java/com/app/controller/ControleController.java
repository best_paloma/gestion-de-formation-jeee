package com.app.controller;



import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.enums.Controlenum;
import com.app.enums.PaiementType;
import com.app.enums.TypeControle;
import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Controle;
import com.app.model.Enseignant;
import com.app.model.Filiere;
import com.app.model.Formation;
import com.app.model.Matiere;
import com.app.model.Module;
import com.app.model.Niveaux;
import com.app.model.Salle;
import com.app.model.Semestre;
import com.app.propertyeditors.AnneeEditor;
import com.app.propertyeditors.CentreEditor;
import com.app.propertyeditors.ControleNumEditor;
import com.app.propertyeditors.ControleTypeEditor;
import com.app.propertyeditors.EnseignantEditor;
import com.app.propertyeditors.FiliereEditor;
import com.app.propertyeditors.FormationEditor;
import com.app.propertyeditors.MatiereEditor;
import com.app.propertyeditors.ModuleEditor;
import com.app.propertyeditors.NiveauEditor;
import com.app.propertyeditors.PaiementTypeEditor;
import com.app.propertyeditors.SalleEditor;
import com.app.propertyeditors.SemestreEditor;
import com.app.service.AnneeService;
import com.app.service.CentreService;
import com.app.service.ControleService;
import com.app.service.EnseignantService;
import com.app.service.FiliereService;
import com.app.service.FormationService;
import com.app.service.LogTrackerService;
import com.app.service.MatiereService;
import com.app.service.ModuleService;
import com.app.service.NiveauxService;
import com.app.service.SalleService;
import com.app.service.SemestreService;

@Controller
public class ControleController {
	
	@Autowired
	MatiereService matiereSvc;
	@Autowired
	AnneeService anneeSvc;
	@Autowired
	EnseignantService enseignantSvc;
	
	@Autowired
	SalleService salleSvc;
	@Autowired
	SemestreService semestreSvc;
	@Autowired
	ControleService controleSvc;
	@Autowired
	NiveauxService niveauSvc;
	@Autowired
	FormationService formationSvc;
	@Autowired
	ModuleService moduleSvc;
	@Autowired
	LogTrackerService logSvc;
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Matiere.class, new MatiereEditor());
        binder.registerCustomEditor(Annee.class, new AnneeEditor());
        binder.registerCustomEditor(Enseignant.class, new EnseignantEditor());
        binder.registerCustomEditor(Salle.class,new SalleEditor() );
        binder.registerCustomEditor(Semestre.class, new SemestreEditor());
        binder.registerCustomEditor(Formation.class, new FormationEditor());
        binder.registerCustomEditor(Module.class, new ModuleEditor());
        binder.registerCustomEditor(Niveaux.class, new NiveauEditor());
        binder.registerCustomEditor(TypeControle.class, new ControleTypeEditor());
        binder.registerCustomEditor(Controlenum.class, new ControleNumEditor());
    }
	
	
	@PreAuthorize("hasRole('CONTROLE_READ')")
	@RequestMapping(value = "/module/controle", method = RequestMethod.GET)
	public String index(Model model) 
	{
		//model.addAttribute("mode", "create");
		
		
		model.addAttribute("liste_matiere", matiereSvc.getAll());
		model.addAttribute("liste_enseignant", enseignantSvc.getAll());
		model.addAttribute("liste_salle", salleSvc.getAll());
		model.addAttribute("liste_annee", anneeSvc.getAll());
		model.addAttribute("liste_module", moduleSvc.getAll());
		model.addAttribute("liste_niveau", niveauSvc.getAll());
		model.addAttribute("liste_formation", formationSvc.getAll());
		model.addAttribute("types_controle", TypeControle.values());
		model.addAttribute("controle_num", Controlenum.values());

		model.addAttribute("semestre", semestreSvc.getAll());
	//	model.addAttribute("controleForm", new 	Controle());
		model.addAttribute("liste_controles", controleSvc.getAll());
		
		return "/module/controle/liste";
	}
	
	
	@PreAuthorize("hasRole('CONTROLE_EDIT')")
	@RequestMapping(value = "/module/controle/ajouter", method = RequestMethod.GET)
	public String ajouter(Model model) 
	{
	
		model.addAttribute("controleForm", new Controle());
		model.addAttribute("liste_matiere", matiereSvc.getAll());
		model.addAttribute("liste_enseignant", enseignantSvc.getAll());
		model.addAttribute("liste_salle", salleSvc.getAll());
		model.addAttribute("liste_annee", anneeSvc.getAll());
		model.addAttribute("liste_module", moduleSvc.getAll());
		model.addAttribute("liste_niveau", niveauSvc.getAll());
		model.addAttribute("liste_formation", formationSvc.getAll());
		model.addAttribute("types_controle", TypeControle.values());
		model.addAttribute("controle_num", Controlenum.values());

		model.addAttribute("semestre", semestreSvc.getAll());
	
		return "module/controle/ajouter";
	}
	
	@PreAuthorize("hasRole('CONTROLE_EDIT')")
	@RequestMapping(value = "/module/controle/saveControle", method = RequestMethod.POST)
	public String saveControle(
			@Valid @ModelAttribute("controleForm") Controle v,
			BindingResult bindingResult,
			@RequestParam(value = "mode", required = true, defaultValue = "create") String mode,
			Model model,
			final RedirectAttributes redirectAttributes,HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("controleForm", v);
			model.addAttribute("salleForm", new Salle());
			model.addAttribute("liste_salle", salleSvc.getAll());
			model.addAttribute("anneeForm", new Annee());
			model.addAttribute("liste_annee", anneeSvc.getAll());
			model.addAttribute("enseignantForm", new Enseignant());
			model.addAttribute("liste_enseignant", enseignantSvc.getAll());
			model.addAttribute("matiereForm", new Matiere());
			model.addAttribute("liste_matiere", matiereSvc.getAll());
			model.addAttribute("liste_module", moduleSvc.getAll());
			model.addAttribute("liste_niveau", niveauSvc.getAll());
			model.addAttribute("liste_formation", formationSvc.getAll());
			model.addAttribute("types_controle", TypeControle.values());
			model.addAttribute("semestreForm", new Semestre());
			model.addAttribute("semestre", semestreSvc.getAll());
			model.addAttribute("controle_num", Controlenum.values());
			return "module/controle/ajouter";
		} else {
			
			if(mode.equals("create"))
			{
				controleSvc.add(v);
				logSvc.store("Ajout Controle: "+v, request);
				redirectAttributes.addFlashAttribute("success_controle", v.getIntitule() + " a ete bien ajouter");
				return "redirect:/module/controle/ajouter";
			}
			else{
				System.out.println("Updating "+v);
				try {
				controleSvc.update(v);
					redirectAttributes.addFlashAttribute("success_controle", v.getIntitule() + " a ete bien modifier");
					return "redirect:/module/controle/ajouter";
				} catch (NotFoundException e) {
					redirectAttributes.addFlashAttribute("error_controle", "Entity not found");
					return "redirect:/module/controle/ajouter";
				}
				
			}
			
		}
	}
	
	@PreAuthorize("hasRole('CONTROLE_READ')")
	@RequestMapping(value = "/module/controle/editControle", method = RequestMethod.GET)
	public String editModule(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Controle p = controleSvc.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("controleForm", p);
		model.addAttribute("salleForm", new Salle());
		model.addAttribute("liste_salle", salleSvc.getAll());
		model.addAttribute("anneeForm", new Annee());
		model.addAttribute("liste_annee", anneeSvc.getAll());
		model.addAttribute("enseignantForm", new Enseignant());
		model.addAttribute("liste_enseignant", enseignantSvc.getAll());
		model.addAttribute("liste_module", moduleSvc.getAll());
		model.addAttribute("liste_niveau", niveauSvc.getAll());
		model.addAttribute("liste_formation", formationSvc.getAll());
		model.addAttribute("matiereForm", new Matiere());
		model.addAttribute("liste_matiere", matiereSvc.getAll());
		model.addAttribute("types_controle", TypeControle.values());
		model.addAttribute("semestreForm", new Semestre());
		model.addAttribute("semestre", semestreSvc.getAll());
		model.addAttribute("controle_num", Controlenum.values());
		return "module/controle/modifier";
	}

	@PreAuthorize("hasRole('CONTROLE_EDIT')")
	@RequestMapping(value = "/module/controle/updatecontrole", method = RequestMethod.POST)
	public  String updatemodule(
			@Valid @ModelAttribute("controleForm") Controle p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("controleForm", p);
			model.addAttribute("salleForm", new Salle());
			model.addAttribute("liste_salle", salleSvc.getAll());
			model.addAttribute("anneeForm", new Annee());
			model.addAttribute("liste_annee", anneeSvc.getAll());
			model.addAttribute("enseignantForm", new Enseignant());
			model.addAttribute("liste_enseignant", enseignantSvc.getAll());
			model.addAttribute("liste_module", moduleSvc.getAll());
			model.addAttribute("liste_niveau", niveauSvc.getAll());
			model.addAttribute("liste_formation", formationSvc.getAll());
			model.addAttribute("matiereForm", new Matiere());
			model.addAttribute("liste_matiere", matiereSvc.getAll());
			model.addAttribute("types_controle", TypeControle.values());
			model.addAttribute("semestreForm", new Semestre());
			model.addAttribute("semestre", semestreSvc.getAll());
			model.addAttribute("controle_num", Controlenum.values());
		
			
			return "modules/controle";
		} else {
			
			try {
				controleSvc.update(p);
				logSvc.store("Modification du Controle: "+p, request);
				redirectAttributes.addFlashAttribute("success_controle", p.getIntitule() + " a ete bien modifier");
				return "redirect:/module/controle/editControle?id="+p.getId();
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error_controle", "Entity not found");
				return "redirect:/module/controle/editControle?id="+p.getId();
			}
			
		}
	}
	
	@PreAuthorize("hasRole('CONTROLE_EDIT')")
	@RequestMapping(value = "/module/controle/deleteControle", method = RequestMethod.GET)
	public String deleteModule(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Controle p = controleSvc.findById(id);
			controleSvc.delete(id);
			logSvc.store("Suppression du Controle: "+p, request);

			redirectAttributes.addFlashAttribute("success_controle_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/module/controle";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_controle_delete", "Entity not found");
			return "redirect:/module/controle";
		}
		
	}
	

}
